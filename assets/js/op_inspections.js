jQuery(document).ready(function($){
  $('#unit-inspections').floatThead({
    scrollContainer: function($table){
      return $table.closest('.table-container');
    }
  });
  var currentInput;
  $('.checkbox input').click(function(e){
    e.preventDefault();
    $('#ready-message').empty();
    currentInput = $(this);
    $('#cancel-ready').text('Cancel');
    $('#dialog-card,#dialog-card-container').addClass('is-open');
  });
  $('#confirm-ready').click(function(){
    var $this = $(this);
    $this.attr('disabled',true);
    var data =  {
      'action': 'op_inspections_mark_unit_as_ready',
      'booking_id': currentInput.val()
    };
    console.log(data);
    $.post( ajaxparams.ajaxurl, data, function (response) {
      console.log(response);
      if(response.success === true) {
        currentInput.prop( "checked", true ).attr('disabled',true);
        currentInput.closest('tr').addClass('disabled');
        $('#ready-message').html('<p class="success">Unit marked as ready'+
        ' and email sent to '+response.data+'.</p>');
        $('#cancel-ready').text('Close');
      }
      else {
        $('#ready-message').html('<p class="failure">There was an error: '+
        response.data+'</p>');
      }
      $this.attr('disabled',false);
    });
  });
  $('#cancel-ready').click(function(){
    currentInput = '';
    $('#dialog-card,#dialog-card-container').removeClass('is-open');
  });
  $('.editable').editable(function(value) {
      var classList = $(this).attr('class').split(/\s+/);
      var field;
      $.each(classList, function(index, item) {
        if (item !== 'editable') {
          field = item;
        }
      });
      var booking_id = $(this).closest('tr').attr('id');
      var data = {
        action: 'op_inspections_update_booking',
        field: field,
        value: value,
        booking_id: booking_id
      };
      $.post( ajaxparams.ajaxurl, data, function (response) {
        console.log(response);
        if(response.success === false) {
          alert(response.data)
        }
      });
      return(value);
    }, {
     type     : 'text',
     submit   : 'OK',
     indicator: 'Saving...',
     onblur   : 'cancel',
     cancel   : 'Cancel',
    }
  );
});
