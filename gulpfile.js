'use strict';

const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const gutil = require('gulp-util');
const babel = require('gulp-babel');
const del = require('del');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');

var paths = {
  vendorScripts: [
    // Specify your vendor scripts in dependency order
    'bower_components/jquery.floatThead/dist/jquery.floatThead.min.js',
    'assets/js/vendor/*.js',
  ],
  appScripts: [
    // You can keep your JS tidy in its own file for a specific feature.
    // Nothing's important about the naming scheme, just as long as the file
    // is included in this array, it'll come together.
    'assets/js/*.js'
  ],
  styles: [
    // do your @imports from this file, not the gulpfile
    'assets/sass/op_inspections.scss'
  ],
  stylesWatchDir: 'assets/sass/**/*.scss'
};

// css tasks

gulp.task('clean:styles', function(){
  return del.sync([
    'dist/css/**/*',
    'dist/images/**/*'
  ]);
});

gulp.task('styles', function() {
  return gulp.src(paths.styles)
    .pipe(plumber(errorHandler))
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/css/'))
    .pipe(cleanCSS({compatibility: '*'}))
    .pipe(rename({
      suffix: ".min",
      extname: ".css"
    }))
    .pipe(gulp.dest('dist/css/'));
});

//js tasks

gulp.task('clean:scripts', function(){
  return del.sync([
    'dist/js/**/*'
  ]);
});

gulp.task('scripts',function() {
  return gulp.src(paths.appScripts)
    .pipe(plumber(errorHandler))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(babel({
      presets: ["es2015"]
    }))
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist/js/'))
    .pipe(uglify().on('error', gutil.log))
    .pipe(rename('app.min.js'))
    .pipe(gulp.dest('dist/js/'))
});

gulp.task('vendor:scripts', function(){
  return gulp.src(paths.vendorScripts)
    .pipe(plumber(errorHandler))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('dist/js/'))
    .pipe(uglify().on('error', gutil.log))
    .pipe(rename({
      suffix: ".min"
    }))
    .pipe(gulp.dest('dist/js/'))
});

//Watch task
gulp.task('default',function() {
  gulp.watch(paths.stylesWatchDir,['clean:styles','styles']);
  gulp.watch(paths.appScripts,['clean:scripts','scripts','vendor:scripts']);
});

function errorHandler(error) {
    // 3 beeps for error
    gutil.log(error);
    gutil.beep();
    return true;
}
