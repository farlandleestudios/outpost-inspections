<?php
/*
Plugin Name: Outpost Unit Inspections
Plugin URI: http://outpostjh.com/
Description: Inspections management for Outpost Rentals
Version: 0.7
Author: Matthew Lee
Author URI: http://farlandlee.com
Text Domain: outpost-inspections
*/

define("INSPECTIONS_VERSION", "0.8");
$outpost_inspections_db_version = INSPECTIONS_VERSION;
define('INSPECTIONS_PATH', plugin_dir_path( __FILE__ ));
define('INSPECTIONS_URL', plugin_dir_url( __FILE__ ));

function op_inspections_activate() {
  global $wpdb,$outpost_inspections_db_version;

  $table_name = $wpdb->prefix . 'op_inspections';
  $wpdb_collate = $wpdb->collate;
  $sql =
  "CREATE TABLE {$table_name} (
  id MEDIUMINT(8) unsigned NOT NULL auto_increment,
  booking_id VARCHAR(50) NOT NULL,
  inspected TINYINT(1) NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY booking_id (booking_id)
  )
  COLLATE {$wpdb_collate}";

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta( $sql );

  add_role( 'unit_inspector', 'Inspector', array( 'read' => true, 'inspect_units' => true ) );

  if( empty( get_site_option( "outpost_inspections_db_version") ) ) flush_rewrite_rules();

  update_option( "outpost_inspections_db_version", $outpost_inspections_db_version );
}
register_activation_hook( __FILE__, 'op_inspections_activate' );
register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );

function outpost_inspections_url_rewrite_templates() {
  if ( is_inspections_page() ) {
    add_filter( 'template_include', function() {
      return INSPECTIONS_PATH . 'templates/page-inspections.php';
    });
  }
}
add_action( 'template_redirect', 'outpost_inspections_url_rewrite_templates' );

function op_inspections_admin_bar(){
  if(is_inspections_page()) {
    return false;
  }
}
add_filter( 'show_admin_bar' , 'op_inspections_admin_bar');

function enqueue_op_inspection_scripts() {
  if(is_inspections_page()) {

    wp_register_script( 'inspections_vendor',  INSPECTIONS_URL . 'dist/js/vendor.js', array('jquery'),INSPECTIONS_VERSION, true );
    wp_enqueue_script('inspections_app', INSPECTIONS_URL . 'dist/js/app.js', array('inspections_vendor','jeditable'),INSPECTIONS_VERSION, true);
    wp_enqueue_style('op_inspections', INSPECTIONS_URL . 'dist/css/op_inspections.css', array(),INSPECTIONS_VERSION, 'all');

    $params = array(
      'ajaxurl'  => admin_url("admin-ajax.php"),
    );
    wp_localize_script( 'inspections_app', 'ajaxparams', $params );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_op_inspection_scripts' );

function is_inspections_page() {
  $re = "/^\\/inspections/i";
	$str = current_page_url();
	$inspections_url = preg_match($re, $str, $matches);
  return $inspections_url;
}

add_action('wp_ajax_nopriv_op_inspections_mark_unit_as_ready','op_inspections_mark_unit_as_ready');
add_action('wp_ajax_op_inspections_mark_unit_as_ready','op_inspections_mark_unit_as_ready');

function op_inspections_mark_unit_as_ready() {
  global $wpdb;
  $booking_id = $_POST['booking_id'];
  // $escapia_options = get_option('escapianet_api');
  // $escapia_account_id = $escapia_options['account_number'];
  $options = get_option('outpost_rentals');
	$cron_mode = $options['cron_mode'];
	$stripe_mode = $options['api_mode'];

  $emailObj = new OP_Email_Builder($wpdb,'unit_ready','',$booking_id );
  $email_address = $emailObj->getEmail();
  $host = $_SERVER['SERVER_NAME'];
	if($stripe_mode === 'production' && $cron_mode == 'on' && $host == 'www.outpostjh.com') $success = $emailObj->send_email();
	else $success = $emailObj->send_email(true,'matthew@farlandlee.com');

  if(!$success) wp_send_json_error( 'Guest email was not sent.' );
  $table = $wpdb->prefix.'op_inspections';
  $data = array (
    'inspected' => 1,
    'booking_id' => $booking_id
  );
  $format = array('%d','%s');
  $test = $wpdb->insert( $table, $data, $format );

  if(!$test) {
    wp_send_json_error( 'Unit not marked as ready in the platform,'.
    ' but guest email was sent to ' . $email_address);
  }

  wp_send_json_success($email_address);
}

add_action('wp_ajax_nopriv_op_inspections_update_booking','op_inspections_update_booking');
add_action('wp_ajax_op_inspections_update_booking','op_inspections_update_booking');

function op_inspections_update_booking() {
  global $wpdb;
  $booking_id = $_POST['booking_id'];
  $field = $_POST['field'];
  $value = $_POST['value'];

  if(empty($booking_id)) wp_send_json_error( 'booking id not set.' );
  elseif(empty($field)) wp_send_json_error( 'field not set.' );

  if($field === 'checkin') $value = date('Y-m-d', strtotime($value));

  $table = $wpdb->prefix.'op_booking';
  $data = array (
    $field => $value
  );
  $where = array(
    'id' => $booking_id
  );
  $test = $wpdb->update( $table, $data, $where );
  if($wpdb->last_error) wp_send_json_error( $wpdb->last_error );

  $value = $value == ''? "empty" : $value;

  if(!$test) wp_send_json_error( "$field not updated to $value");

  wp_send_json_success("$field updated to $value");
}

function outpost_inspections_update_db_check() {
  global $outpost_inspections_db_version;

  if ( get_site_option( "outpost_inspections_db_version" ) != $outpost_inspections_db_version ) {
    op_inspections_activate();
  }
}
add_action( 'plugins_loaded', 'outpost_inspections_update_db_check' );

?>
