<?php
/**
 * This file adds the Special Landing template to Outpost.
 *
 * @author Matthew Lee
 * @package Outpost
 * @subpackage Customizations
 */

add_filter( 'body_class', 'op_add_body_class' );
function op_add_body_class( $classes ) {

  $blacklist = array( 'home', 'blog', 'archive', 'single', 'category', 'tag', 'error404', 'content-sidebar' );

  $classes = array_diff( $classes, $blacklist );

	$classes[] = 'inspections';
	return $classes;

}

add_filter('show_admin_bar', '__return_false');

add_filter('wp_title', 'op_inspections_custom_title',999);
function op_inspections_custom_title($title) {
  return "Outpost Unit Inspections | Outpostjh.com";
}
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php
      if(!current_user_can( 'inspect_units' ) && !current_user_can( 'manage_options' )) {
        auth_redirect();
      }
    ?>
		<?php wp_head(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE" />
	</head>
	<body <?php body_class(); ?>>
    <header data-role="header" class="ui-header">
      <h1 class="ui-title">Unit Inspections</h1>
    </header>
    <main id="main-content" class="main-content">
      <div class="table-container table-scroll">
        <table class="unit-inspections" id="unit-inspections">
          <thead>
            <tr>
              <th>Ready</th>
              <th>Unit Code</th>
              <th>Checkin</th>
              <th>Telephone</th>
              <th>Last Name</th>
            <tr>
          </thead>
          <tbody>
          <?php
            $escapia_options = get_option('escapianet_api');
            $escapia_account_id = $escapia_options['account_number'];
            $sql = "SELECT bd.booking_id,
            escapia_property_id,
            checkin,
            checkout,
            email,
            primary_phone,
            first_name,
            last_name,
            unit_code,
            p.post_title as unit_name,
            i.inspected as inspected
            FROM {$wpdb->prefix}op_booking_data bd
            LEFT JOIN {$wpdb->prefix}posts p
            ON (rental_id = p.ID)
            LEFT JOIN {$wpdb->prefix}op_inspections i
            USING (booking_id)
            WHERE checkin >= '2016-11-21'
            AND type LIKE 'RES%'
            GROUP BY (bd.id)
            ORDER BY checkin";
            $bookings = $wpdb->get_results($sql);
            echo $wpdb->last_error;
            if($bookings) {
              foreach($bookings as $booking) {
                echo '<tr id="'.$booking->id.'"';
                if($booking->inspected) echo ' class="disabled"';
                echo '>';
                echo '<td class="checkbox unit-ready"><input type="checkbox" id="ready-'.
                $booking->booking_id.'" name="ready['.$booking->booking_id.']" value="'.$booking->booking_id.'" ';
                checked($booking->inspected,1,true);
                if($booking->inspected) echo ' disabled="disabled"';
                echo ' ><label for="ready-'.$booking->booking_id.'"'.' ></label></td>';
                echo '<td class="unit_code editable">';
                if(!empty($booking->unit_code)) echo $booking->unit_code;
                else echo $booking->ucode;
                echo '</td>';
                echo '<td class="checkin editable">'.date('m/d/y',strtotime($booking->checkin)).'</td>';
                echo '<td class="telephone editable">'.$booking->primary_phone.'</td>';
                echo '<td class="lastname editable">'.$booking->last_name.'</td>';
                echo '</tr>';
              }
            }
          ?>
          </tbody>
        </table>
      </div>
    </main>
    <footer>
      <div class="logo"></div>
      <div class="telephone">
        <a href="tel:3076904790">307.690.4790</a>
      </div>
    </footer>
    <?php wp_footer(); ?>
    <div id="dialog-card-container">
      <div id="dialog-card">
        <div class="top">
          <h1>Confirm</h1>
          <div id="ready-message"></div>
          <p>This will mark the unit as ready and send an email to the renter.</p>
        </div>
        <div class="bottom">
          <button type="button" class="success button" id="confirm-ready">Confirm</button>
          <button type="button" class="alert button" id="cancel-ready">Cancel</button>
        </div>
      </div>
    </div>
  </body>
</html>
